/*
 * gpc_host.cpp
 *
 * host library (XRT runtime version)
 *
 *  Created on: April 23, 2021
 *      Author: A.Popov
 */

#include "gpc_xrt_host.h"


// Constructor
gpc::gpc(xrt::device *xrt_device,xrt::uuid *xrt_uuid,unsigned int group_number,unsigned int core_number)
{
	group=group_number;
	core=core_number;
	device=xrt_device;
	uuid=xrt_uuid;
	ip = new xrt::ip(*device, *uuid, LNH_CORE_DEFS.KERNEL_NAME[group][core]);
	//auto interrupt = ip->create_interrupt_notify()
	//interrupt = new xrt::ip::interrupt(ip->create_interrupt_notify());
};

gpc::~gpc() {
	free(ip);
}

unsigned int gpc::load_file_to_memory(const char *filename, char **result)
{
	unsigned int size = 0;
	FILE *f = fopen(filename, "rb");
	if (f == NULL)
	{
		*result = NULL;
		return -1; // -1 means file opening fail
	}
	fseek(f, 0, SEEK_END);
	size = ftell(f);
	fseek(f, 0, SEEK_SET);
	if (size != fread(*result, sizeof(char), size, f))
	{
		free(*result);
		return -2; // -2 means file reading fail
	}
	fclose(f);
	(*result)[size] = 0;
	return size;
}


// Load RV kernel binary to the GPN
void gpc::load_sw_kernel(char *gpcbin,xrt::bo *global_memory_ptr, xrt::bo *ddr_memory_ptr)
{
	//Set buffers
	ddr_memory_pointer = ddr_memory_ptr;
	global_memory_pointer = global_memory_ptr;
	long long ddr_memory_address = ddr_memory_pointer->address();
	long long global_memory_address = global_memory_pointer->address();
	//Write sw_kernel to global memory
	char *gpc_kernel = global_memory_pointer->map<char*>();
	unsigned int n_gpc0 = load_file_to_memory(gpcbin, (char **)&gpc_kernel);
	global_memory_pointer->sync(XCL_BO_SYNC_BO_TO_DEVICE, n_gpc0, 0);
	//Set standard registers
	ip->write_register(G_IE_OFFSET,G_IE_ENABLE);
	ip->write_register(IP_IE_OFFSET,AP_DONE_IE_ENABLE);
	//Reset CPE
	ip->write_register(LNH_CONFIG_OFFSET, LNH_CORE_DEFS.LEONHARD_CONFIG[group][core]);
	ip->write_register(GPC_RESET_OFFSET, GPC_RESET_HIGH);
	ip->write_register(GPC_CONFIG_OFFSET, n_gpc0);
	ip->write_register(DDR4_BUS_PTR_OFFSET, ddr_memory_address);
	ip->write_register(DDR4_BUS_PTR_OFFSET+4, ddr_memory_address>>32);
	ip->write_register(GLOBAL_MEMORY_PTR_OFFSET, global_memory_address);
	ip->write_register(GLOBAL_MEMORY_PTR_OFFSET+4, global_memory_address>>32);
	//Start bootloader
	//auto interrupt = ip->create_interrupt_notify();
	ip->write_register(CONTROL_OFFSET,AP_START);
    // Wait until the GPC is DONE
    unsigned int axi_ctrl = 0;
    while((axi_ctrl & AP_IDLE) != AP_IDLE) {
        axi_ctrl = ip->read_register(CONTROL_OFFSET);
    }
	//Enable handlers execution
	ip->write_register(GPC_RESET_OFFSET, GPC_RESET_LOW);
}


void gpc::start_sync(const unsigned int event_handler)
{
	//auto interrupt = ip->create_interrupt_notify();
	//Set args for RUN context
	ip->write_register(GPC_CONFIG_OFFSET, event_handler);
	//Clear Interrupt status
	ip->read_register(IP_IS_OFFSET);
	//Start kernel
	ip->write_register(CONTROL_OFFSET,AP_START);
	//Wait for gpc ready
	//interrupt.wait();
    unsigned int axi_ctrl = 0;
    while((axi_ctrl & AP_IDLE) != AP_IDLE) {
        axi_ctrl = ip->read_register(CONTROL_OFFSET);
    }
}

void gpc::start_async(const unsigned int event_handler)
{
	//Set args for RUN context
	ip->write_register(GPC_CONFIG_OFFSET, event_handler);
	//Clear Interrupt status
	ip->read_register(IP_IS_OFFSET);
	//Start kernel
	ip->write_register(CONTROL_OFFSET,AP_START);
}

void gpc::finish()
{
	//Wait for gpc ready
	//auto interrupt = ip->create_interrupt_notify();
	//interrupt.wait();
    unsigned int axi_ctrl = 0;
    while((axi_ctrl & AP_IDLE) != AP_IDLE) {
        axi_ctrl = ip->read_register(CONTROL_OFFSET);
    }

}



