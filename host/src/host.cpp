

#include <iostream>
#include <stdio.h>
#include <stdexcept>
#ifdef _WINDOWS
#include <io.h>
#else
#include <unistd.h>
#include <sys/time.h>
#endif


#include "experimental/xrt_device.h"
#include "experimental/xrt_kernel.h"
#include "experimental/xrt_bo.h"
#include "experimental/xrt_ip.h"
#include "experimental/xrt_ini.h"

#include "gpc_defs.h"
#include "leonhardx64_xrt.h"
#include "gpc_handlers.h"

static void usage()
{
	std::cout << "usage: <xclbin> <sw_kernel>\n\n";
}


int main(int argc, char** argv)
{

	unsigned int err = 0;
	unsigned int cores_count = 0;
	float LNH_CLOCKS_PER_SEC;

	__foreach_core(group, core) cores_count++;

	//Assign xclbin
	if (argc < 3) {
		usage();
		throw std::runtime_error("FAILED_TEST\nNo xclbin specified");
	}

	//Open device #0
	leonhardx64 lnh_inst = leonhardx64(0,argv[1]);
	__foreach_core(group, core)
	{
		lnh_inst.load_sw_kernel(argv[2], group, core);
	}

	/*
	 *
	 * GPC frequency measurement for the first kernel
	 *
	 */
	lnh_inst.gpc[0][LNH_CORES_LOW[0]]->start_async(__event__(frequency_measurement));
	// Measurement Body
	lnh_inst.gpc[0][LNH_CORES_LOW[0]]->sync_with_gpc(); // Start measurement
	sleep(1);
	lnh_inst.gpc[0][LNH_CORES_LOW[0]]->sync_with_gpc(); // Start measurement
	// End Body
	lnh_inst.gpc[0][LNH_CORES_LOW[0]]->finish();
	LNH_CLOCKS_PER_SEC = (float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive();
	printf("Leonhard clock frequency (LNH_CF):     %.1f MHz\n", LNH_CLOCKS_PER_SEC / 1000000);

	/*
	 *
	 * SW Kernel Version and Status
	 *
	 */
	__foreach_core(group, core)
	{
		printf("Group #%d \tCore #%d\n", group, core);
		lnh_inst.gpc[group][core]->start_sync(__event__(get_version));
		printf("\tSoftware Kernel Version:\t0x%08x\n", lnh_inst.gpc[group][core]->mq_receive());
		lnh_inst.gpc[group][core]->start_sync(__event__(get_lnh_status_high));
		printf("\tLeonhard Status Register:\t0x%08x", lnh_inst.gpc[group][core]->mq_receive());
		lnh_inst.gpc[group][core]->start_sync(__event__(get_lnh_status_low));
		printf("_%08x\n", lnh_inst.gpc[group][core]->mq_receive());
	}


	//-------------------------------------------------------------
	// Измерение производительности Leonhard
	//-------------------------------------------------------------

	float interval;
	char buf[100];
	err = 0;

	time_t now = time(0);
	strftime(buf, 100, "Start at local date: %d.%m.%Y.; local time: %H.%M.%S", localtime(&now));

	printf("DISC system speed test v3.0\n%s\n\n", buf);
	printf("Test                                    value (units)\n");

	/*
	 *
	 * Memory write rate
	 *
	 */
	lnh_inst.gpc[0][LNH_CORES_LOW[0]]->start_sync(__event__(test0));
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / LNH_CLOCKS_PER_SEC;
	printf("Average RISCV memory write rate (AMWR64): %.1f (ops./sec.)\n", (float)1000000 / interval);

	/*
	 *
	 * Bus write rate
	 *
	 */
	lnh_inst.gpc[0][LNH_CORES_LOW[0]]->start_sync(__event__(test1));
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / LNH_CLOCKS_PER_SEC;
	printf("Average RISCV AXL bus write rate (ABWR64):%.1f (ops./sec.)\n", (float)1000000 / interval);


	/*
	 *
	 * Kernel RUN rate
	 *
	 */

	clock_t start,stop;

	start=clock();
	for (int i=0; i<1000; i++)
		__foreach_core(group, core)
			lnh_inst.gpc[group][core]->start_async(0);
	stop=clock();
	printf("Average sw_kernel Run Rate (AKRR): %.1f (calls/sec.)\n", float(1000 * CLOCKS_PER_SEC * cores_count) / float(stop-start));


	/*
	 *
	 * Queue messaging rate
	 *
	 */

	unsigned int *host2gpc_buffer[LNH_GROUPS_COUNT][LNH_MAX_CORES_IN_GROUP];
	__foreach_core(group, core)
	{
		host2gpc_buffer[group][core] = (unsigned int*) malloc(1024*sizeof(int));
		for (int i=0;i<1024;i++) host2gpc_buffer[group][core][i] = i;
	}
	unsigned int *gpc2host_buffer[LNH_GROUPS_COUNT][LNH_MAX_CORES_IN_GROUP];
	__foreach_core(group, core)
	{
		gpc2host_buffer[group][core] = (unsigned int*) malloc(1024*sizeof(int));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(mq_rate_test));
	}
	start=clock();
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->mq_send(1024*sizeof(int),host2gpc_buffer[group][core]);
	}
	__foreach_core(group, core)
		{
		lnh_inst.gpc[group][core]->mq_receive(1024*sizeof(int),gpc2host_buffer[group][core]);
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->mq_send_join();
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->mq_receive_join();
	}
	stop=clock();
	__foreach_core(group, core) {
		free(host2gpc_buffer[group][core]);
		free(gpc2host_buffer[group][core]);
	}
	printf("Average Message Queue Send-Receive Rate:\t%.1f (messages/sec.)\n", float(1024 * CLOCKS_PER_SEC * cores_count) / float(stop-start));

	/*
	 *
	 * Queue messaging rate
	 *
	 */

	__foreach_core(group, core)
	{
		host2gpc_buffer[group][core] = (unsigned int*) malloc(1024*sizeof(int));
		for (int i=0;i<1024;i++) host2gpc_buffer[group][core][i] = i;
	}
	__foreach_core(group, core)
	{
		gpc2host_buffer[group][core] = (unsigned int*) malloc(1024*sizeof(int));
	}
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->start_async(__event__(buf_rate_test));
	}
	start=clock();
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->buf_write(1024*sizeof(int),host2gpc_buffer[group][core]);
	}
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->buf_write_join();
	}
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->mq_send(1024*sizeof(int));
	}
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->mq_receive();
	}
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->buf_read(1024*sizeof(int),gpc2host_buffer[group][core]);
	}
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->buf_read_join();
	}
	stop=clock();
	__foreach_core(group, core) {
		free(host2gpc_buffer[group][core]);
		free(gpc2host_buffer[group][core]);
	}
	printf("Average BufferIO Send-Receive Rate:\t%.1f (bytes/sec.)\n", float(4096*CLOCKS_PER_SEC*cores_count) / float(stop-start));


	/*
	 *
	 * Average time of the sequential insert
	 *
	 */
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test2));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("Average sequential insert rate (ASIR64):  %.1f (ops./sec.)\n", (float)1000000 / interval);

	/*
	 *
	 * Average time of the random insert
	 *
	 */
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test3));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("Average random insert rate (ARIR64):      %.1f (ops./sec.)\n", (float)1000000 / interval);

	/*
	 *
	 * Sequential search
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test4));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("Average sequential search rate (ASSR64):  %.1f (ops./sec.)\n", (float)1000000 / interval);

	/*
	 *
	 * Random search
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test5));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("Average random search rate (ARSR64):      %.1f (ops./sec.)\n", (float)1000000 / interval);

	/*
	 *
	 * Average sequential delete rate
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test6));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("Average sequential delete rate (ASDR64):  %.1f (ops./sec.)\n", (float)1000000 / interval);

	/*
	 *
	 * Average random delete rate
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test7));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("Average random delete rate (ARDR64):      %.1f (ops./sec.)\n", (float)1000000 / interval);

	/*
	 *
	 * Local memory 1M keys traversing time
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test8));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("LSM 1M keys traversing time (LSMTT64):    %.6f (msec.)\n", 1000 * interval);

	/*
	 *
	 * Average neighbours search rate
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test9));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("Average neighbours search rate (ANSR64):  %.1f (ops./sec.)\n", (float)1000000 / interval);

	/*
	 *
	 * 1M dataset AND time
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test10));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("1M dataset AND time (ANDT64):             %.6f (msec.)\n", 1000 * interval);

	/*
	 *
	 * 1M datasets OR time
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test11));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("1M dataset OR time (ORT64):               %.6f (msec.)\n", 1000 * interval);
	/*
	 *
	 * 1M dataset NOT time
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test12));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("1M dataset NOT time (NOTT64):             %.6f (msec.)\n", 1000 * interval);

	/*
	 *
	 * 1M dataset Slice time
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test13));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("1M dataset slice time (SLCT64):           %.6f (msec.)\n", 1000 * interval);

	/*
	 *
	 * Worst possible insert time / Full LSM shift
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test14));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("Worst possible insert time (WPIT64):      %.3f (msec.)\n", 1000 * interval);

	/*
	 *
	 * 1M dataset squiz time
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(test15));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst.gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("1M dataset squiz rate (SQZT64):           %.6f (sec.)\n", interval);

	/*
	 *
	 * Experiment 1
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp1));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #1. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 2
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp2));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #2. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 3
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp3));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #3. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 4
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp4));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #4. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 5
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp5));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #5. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 5
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp5));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #5. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 6
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp6));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #6. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 7
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp7));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #7. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 8
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp8));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #8. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 9
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp9));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #9. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 10
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(exp10));
	}
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->finish();
		printf("Test #10. Error count:                 	%d\n", lnh_inst.gpc[group][core]->mq_receive());
	}

	now = time(0);
	strftime(buf, 100, "Stop at local date: %d.%m.%Y.; local time: %H.%M.%S", localtime(&now));
	printf("DISC system speed test v1.1\n%s\n\n", buf);

	//--------------------------------------------------------------------------
	// Shutdown and cleanup
	//--------------------------------------------------------------------------

	if (err)
	{
		printf("ERROR: Test failed\n");
		return EXIT_FAILURE;
	}
	else
	{
		printf("INFO: Test completed successfully.\n");
		return EXIT_SUCCESS;
	}





	return 0;
}
