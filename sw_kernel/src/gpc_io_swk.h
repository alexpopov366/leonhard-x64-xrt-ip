/*
 * gpc_io_swk.h
 *
 * sw_kernel library
 *
 *  Created on: April 23, 2021
 *      Author: A.Popov
 */

#ifndef GPC_IO_H_
#define GPC_IO_H_

#include "gpc_swk.h"

//====================================
// Адресация ресурсов на шине AXI
//====================================
#define MSG_BLOCK_START 0x00010000                              // area for IO

// Check parameters to avoid global memory conflict
// 1. MSG_BUFFER_SIZE == 2 * (MSG_QUEUE_FSIZE + MSG_BUFFER_SIZE)
// 2. MSG_BLOCK_SIZE * (GPC cores in GROUP, 6 is default) < 64K
#define MSG_BLOCK_SIZE  0x2800                                  // 10K Block size for every GPC
#define MSG_QUEUE_FSIZE 0x400                                   // 1024 bytes for queues
#define MSG_BUFFER_SIZE 0x1000                                  // 4096 bytes buffers for GPC IO

// Automatically calculated parameters
#define MSG_QUEUE_SIZE  (MSG_QUEUE_FSIZE - 2 * sizeof(int))     // 1016 bytes for data, 8 bytes for pointers
#define HOST2GPC_QUEUE  0                                       // Offset for Host2GPC queue
#define GPC2HOST_QUEUE  MSG_QUEUE_FSIZE                         // Offset for GPC2Host queue
#define HOST2GPC_BUFFER (2*MSG_QUEUE_FSIZE)                     // Offset for Host2GPC queue
#define GPC2HOST_BUFFER (HOST2GPC_BUFFER + MSG_BUFFER_SIZE)     // Offset for GPC2Host queue


//===================================
// Типа данных и структуры для работы
//===================================

// Define Leonhard x64 descriptor
typedef struct
    global_memory_io
{
        // Queue base offsets
        u32 gpc2host_queue;
        u32 host2gpc_queue;
        // Buffers offsets
        u32 gpc2host_buffer;
        u32 host2gpc_buffer;
        // gpn2host queue pointers
        u32 gpc2host_head_offset; // updated by gpc
        u32 gpc2host_tail_offset; // updated by host
        // host2gpn queue pointers
        u32 host2gpc_head_offset; // updated by host
        u32 host2gpc_tail_offset; // updated by gpc
} global_memory_io;

#ifdef DEFINE_GM_IO
#define EXTERN /* nothing */
#else
#define EXTERN extern
#endif /* DEFINE_GM_IO */
EXTERN global_memory_io gmio;

//========================================
// Функции для обмена данными с HOST
//========================================

void gmio_init(int core);
void mq_send(unsigned int message);
unsigned int mq_receive();
void buf_read(unsigned int size, unsigned int *local_buf);
void buf_write(unsigned int size, unsigned int *local_buf);
void sync_with_host();

#endif
